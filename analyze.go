package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

const (
	flagSeverityLevel = "severity-level"

	pathOutput     = "/tmp/flawfinder.csv"
	pathFlawfinder = "/usr/local/bin/flawfinder"
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{
		&cli.IntFlag{
			Name:    flagSeverityLevel,
			Usage:   "Severity level",
			EnvVars: []string{"SAST_FLAWFINDER_LEVEL"},
			Value:   1,
		},
	}
}

// ReadCloser wraps a Reader and implements a Close methods that does nothing.
type ReadCloser struct{ io.Reader }

// Close is a fake implementation.
func (r ReadCloser) Close() error {
	return nil
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	level := strconv.Itoa(c.Int(flagSeverityLevel))
	cmd := exec.Command(pathFlawfinder, "-m", level, "--csv", ".")
	cmd.Dir = path
	cmd.Env = os.Environ()
	cmd.Stderr = os.Stderr

	output, err := cmd.Output()
	log.Debugf("%s\n%s", cmd.String(), output)
	if err != nil {
		return nil, err
	}

	return ioutil.NopCloser(bytes.NewReader(output)), nil
}
